- Tea Break : 11:00 (15 Minutes)
- Lunch Break : 01:00 (45 Minutes)
- Tea Break : 03:30 (15 Minutes)

# What is JavaScript?

- Dynamic data types
- After running/executing the scripts, we are able to receive errors

# TypeScript

- String, Number, Boolean, Array, Date, Function, Object
- Any, Enum, Tuple, Never, Unknown, Void
- Custom Types : type keyword, interface, classes
- Next-Gen JavaScript (ESM, Arrow, Spread/Rest, Destructuring, Promise, Map, Set etc)
- Extension for JavaScript with Type Syntax
- Auto-wiring / Dependency Injection

# What is Angular ?

- Collection of various Libraries/packages
- Single Page Apps : @angular/router
- Form Validation : @angular/forms
- Animation : @angular/animation
- JavaScript Framework
- Component based architecture
- Developed and sponsured by Google Team
- Unit Testing
- Can be used to develop Web, Mobile and Hybrid Apps
- MVC Architecture
- Two way data binding
- Bootstrap and Material Support
- Client Side App
- Not browser specific
- Angular -> Latest Version -> Framework
- AngularJS -> v1.x -> Library
- Angular Universal : Server-Side Rendering

# Other JS Libraries/Frameworks

- React : to render the UI quickly and efficiently, VDOM, Props, State Management etc
  SPA using react-router-dom, State Management using Redux/RTK, Form Validation using formik, react-form-hook, yup etc
- Preact : sub-set of React
- \*Vue : Progressive framework, MVC, 2 way data binding, Template syntax, State Management, UI rendering etc
  State Management using Vuex, Form Validation using vuelidate
- Knockout : MVVM Pattern, Two way data binding
- jQuery : DOM manipulation, AJAX, Animation
- Backbone : MVC at Client side
- Polymer : Custom Web Components
- Stensil : VDOM, Custom Web Components
- Anim : Animation

# Server-Side Scripts

- NodeJS
- Express

# install Angular CLI Tool

- npm install -g @angular/cli@14
- ng --help
- ng version
- ng new <proj-name>
- cd frontend
- npm run start | ng serve

- npm uninstall -g @angular/cli
- npm i -g @angular/cli@14.2

# Node Installer

- Node Package Manager (NPM) - 6.x
- Node Runtime Environment (NRE) - 14.x
- Node Native Modules

Ng Version 11 -> Node Version v12 -> NPM v6
Ng v14 -> Node v14 -> NPM v6
Ng v15 -> Node LTS -> NPM v8

# Angular Building Blocks

- Components : ES6 Class + @Component({})
- Services : ES6 Class + @Injectable({})
- Directives : ES6 Class + @Directive({})
- Pipes : ES6 Class + @Pipe({})
- Modules :ES6 Class + @NgModule({})

# Generate Command

- ng generate component path/to/component/ComponentName
- ng generate directive directiveName
- ng g c componentName
- ng g d directiveName
- ng g s serviceName
- ng g p pipeName
- ng g m moduleName

# Install Bootstrap

> npm i bootstrap

# View Encapsulation - implements the SHADOW DOM concepts

- Emulated (Default) : Local CSS will override the Global CSS Rule
- None : Local CSS will affect the other part of your app
- ShadowDOM / Native : Only Local CSS will affect the component template

Comp A - None
Comp B - ShadowDOM

# Component Types

- Smart / Parent / Container : contains business logic eg. users component
- Dumb / Child / Presentational : receives data from parent component and creates the UI. eg. user info, user image

# Component Lifecycle Methods/Hooks

- ngOnChanges : when component receive the Data from Parent using @Input()
- ngOnInit
- ngDoCheck : runs for every change detection
- ngAfterContentInit : Projected Content is initialized
- ngAfterContentChecked : Projected Content is settled.
- ngAfterViewInit : View is Initialized; Self + Child Component View is initialized
- ngAfterViewChecked : runs when the view is settled
- ngOnDestroy : just before component is about to destroy

Comp A -> Comp B

# ViewChild ->

- able to access the view elements after the component view is initialized
- Not able to access the projected contents

# ContentChild ->

- able to access the projected content after the content is intialized

# Directives

- Attribute : ngClass, ngStyle
- Structural : *ngFor, *ngIf, \*ngSwitch

- Custom Directive

  > @HostBinding
  > @HostListener

- Primitive Types (Immutability): String, Number, Boolean
- Reference Types (Mutability): Object, Array, Function, Date

- Mutability & Immutability

let arr = [];

- Mutable Change : does not change the reference in memory
- Impure Changes
  arr.push("apple")

-Immutable Change : memory address is changed

- Pure Changes - Pure Pipes will run
  arr = ["apple", "banana"];

  let obj = {}

  obj.age = 32; ---> impure change

  obj = {age : 32} ---> pure change

  # States / CSS Classes to Form / Form Elements

  - ngValid / ngInvalid
  - ngPristine / ngDirty
  - ngTouched / ngUntouched

- FormGroup : combo of various formControls, formArrays
- FormArray : Combo of various formControls, formGroups

hobbies = [
new FormGroup({ name : "", frequency : ""}),
new FormGroup({ name : "", frequency : ""}),
new FormGroup({ name : "", frequency : ""}),
new FormGroup({ name : "", frequency : ""}),
]

# Observables : keeps an eye on data source

- stream on which event emitted at different time interval
- are both Async and Sync
- are lazily executed
- are cancelable
- powerful operators

# Promise - Success (resolved) / Failure (rejected)

- to handle async behaviour
- are Asynchronous only
- are eagerly executed
- are NOT cancelable
- one shot

# Observable :

- subscribe() - used to consume the data coming in observable stream
- pipe() - used to attach various operators

# Observer :

- next() - used to send the data on the observable stream
- error()
- complete()

# Subjects : are both - Observable + Observer

- subsrcibe()
- pipe()
- next()
- error()
- complete()

# Types of Subject :

> BehaviourSubject
> AsyncSubject
> ReplaySubject

> npm i json-server -g
> created data/db.json (outside frontend)
> convert users data into JSON {{ users | json }}
> copy and paste JSON data in db.json file (Can also use db.json available in project folder on Gitlab)
> json-server db.json --watch - spit out REST endpoint

> reactivex.io
> rxjs.dev

# Forms in Angular

- Template driven forms
- Reactive / Model driven forms
  > ReactiveFormsModule in Root Module
  > FormControl, FormGroup, FormArray

# Services : contains business logic

- Hierarchical Injection
- Singleton Instance
- De-couple logic

# HttpClient

# Observables : streams on which event occur at different time interval

- Observer : next, error, completed
- Observable : pipe, subscribe

- from, interval, new Observable(), of, fromEvent, range etc
- Operators Types - Creational, Filtering, Transforming, Utility, Erro Handling etc

# Subject

# Interceptor : used to intercept outgoing request and incoming responses

# Routing : Single Page App

# Multi-Module

# Routing Terminologies

- Routes : Configure the routes with the components
- RouterModule : enable routing in app
- <router-outlet> : provides the space on template to load the component
- routerLink : change the URL without reloading the page
- Router Service : programmtically navigate the user
- ActivatedRoute : access of current URL

- Preloading Strategy : to preload all lazy loaded modules
- canload : use with lazy module loading
- canActivate : use with component loading
- resolve : use to pre-fetch the data (before loading the component template)
