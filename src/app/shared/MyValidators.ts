import { AbstractControl } from "@angular/forms";

export class MyValidators {
  static hasExclamation(control: AbstractControl<any>) {
    const hasExcl = control.value.indexOf("@") >= 0;
    return hasExcl ? null : { needAtSign: true };
  }
}
