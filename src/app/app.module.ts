import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, PreloadAllModules } from '@angular/router';

import { AppComponent } from './app.component';
import { DemoComponent } from './components/demo/demo.component';
import { UsersComponent } from './components/users/users.component';
import { UserImageComponent } from './components/users/user-image/user-image.component';
import { UserInfoComponent } from './components/users/user-info/user-info.component';
import { AlertComponent } from './components/alert/alert.component';
import { LifeCycleDemoComponent } from './components/life-cycle-demo/life-cycle-demo.component';
import { CommentsComponent } from './components/users/comments/comments.component';
import { HighlightDirective } from './directives/highlight.directive';
import { TestDirective } from './directives/test.directive';
import { PipeDemoComponent } from './components/pipe-demo/pipe-demo.component';
import { CountryCodePipe } from './pipes/country-code.pipe';
import { ReversePipe } from './pipes/reverse.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { CommentFormComponent } from './components/users/comment-form/comment-form.component';
import { AuthComponent } from './components/auth/auth.component';
import { DataService } from './services/data.service';
import { ObservableDemoComponent } from './components/observable-demo/observable-demo.component';
import { LoggerInterceptor } from './services/interceptors/logger.interceptor';
import { APP_ROUTES } from './app.routes';
import { HeaderComponent } from './components/header/header.component';
import { PostComponent } from './components/post/post.component';
import { PostItemComponent } from './components/post-item/post-item.component';
import { AddPostComponent } from './components/add-post/add-post.component';
import { EmployeeModule } from './modules/employee/employee.module';

@NgModule({
  declarations: [       // Component, Directive, Pipes
    AppComponent,
    DemoComponent,
    UsersComponent,
    UserImageComponent,
    UserInfoComponent,
    AlertComponent,
    LifeCycleDemoComponent,
    CommentsComponent,
    HighlightDirective,
    TestDirective,
    PipeDemoComponent,
    CountryCodePipe,
    ReversePipe,
    FilterPipe,
    CommentFormComponent,
    AuthComponent,
    ObservableDemoComponent,
    HeaderComponent,
    PostComponent,
    PostItemComponent,
    AddPostComponent
  ],
  imports: [            // Modules - Built-in / Custom Module
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(APP_ROUTES, { preloadingStrategy: PreloadAllModules }),
    EmployeeModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: LoggerInterceptor,
    multi: true
  }],        // Service
  // providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
