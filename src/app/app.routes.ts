import { Routes } from '@angular/router';
import { AddPostComponent } from './components/add-post/add-post.component';
import { AuthComponent } from './components/auth/auth.component';
import { ObservableDemoComponent } from './components/observable-demo/observable-demo.component';
import { PipeDemoComponent } from './components/pipe-demo/pipe-demo.component';
import { PostItemComponent } from './components/post-item/post-item.component';
import { PostComponent } from './components/post/post.component';
import { UsersComponent } from './components/users/users.component';
import { PostResolver } from './resolvers/post.resolver';
import { LoadGuard } from './services/guard/load.guard';
import { LoginGuard } from './services/guard/login.guard';

export const APP_ROUTES: Routes = [
  {
    path: "",                             // http://localhost:4200
    redirectTo: "/auth",
    pathMatch: "full"
  }, {
    path: "auth",                             // http://localhost:4200/auth
    component: AuthComponent
  }, {
    path: "users",                           // http://localhost:4200/users
    component: UsersComponent,
    canActivate: [LoginGuard]
  }, {
    path: "pipe-demo",                            // http://localhost:4200/pipe-demo
    component: PipeDemoComponent
  }, {
    path: "observable-demo",                       // http://localhost:4200/observable-demo
    component: ObservableDemoComponent
  }, {
    path: "posts",                          // http://localhost:4200/posts
    component: PostComponent,
    children: [{
      path: "add-post",
      component: AddPostComponent
    }, {
      path: ":postId",                     // http://localhost:4200/posts/postId
      resolve: { postData: PostResolver },
      component: PostItemComponent
    }]
  }, {
    path: "lazy",                        // Loading module lazily / on-demand
    canLoad: [LoadGuard],
    loadChildren: () => import("./modules/lazy/lazy.module").then(m => m.LazyModule)
  }, {
    path: "**",                                  // http://localhost:4200/doesnotexist
    redirectTo: "/auth",
    pathMatch: 'full'
  }
]
