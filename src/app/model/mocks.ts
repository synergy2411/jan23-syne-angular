import { IUser } from "./user.interface";

export const USER_DATA: IUser[] = [{
  firstName: "bill",
  lastName: "gates",
  dob: new Date("Dec 21, 1965"),
  isWorking: true,
  income: 50000,
  totVotes: 120,
  image: "https://pbs.twimg.com/profile_images/1564398871996174336/M-hffw5a_400x400.jpg",
  comments: [
    { id: "c001", stars: 4, body: "Great work 👍", author: "foo@test" },
    { id: "c002", stars: 3, body: "Nice work", author: "bar@test" },
  ]

}, {
  firstName: "tim b.",
  lastName: "lee",
  dob: new Date("Jan 1, 1966"),
  isWorking: true,
  income: 30000,
  totVotes: 180,
  image: "https://cdn.britannica.com/22/221822-050-3B0A657F/British-scientist-Tim-Berners-Lee.jpg",
  comments: [
    { id: "c003", stars: 5, body: "Luv your work", author: "xyz@test" }
  ]
}, {
  firstName: "steve",
  lastName: "jobs",
  dob: new Date("Aug 16, 1968"),
  isWorking: false,
  income: 0,
  totVotes: 240,
  image: "https://upload.wikimedia.org/wikipedia/en/thumb/e/e4/Steve_Jobs_by_Walter_Isaacson.jpg/220px-Steve_Jobs_by_Walter_Isaacson.jpg",
  comments: []
}]
