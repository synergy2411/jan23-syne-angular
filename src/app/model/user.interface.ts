import { IComment } from "./comment.interface";

export interface IUser {
  firstName: string;
  lastName: string;
  dob: Date;
  isWorking: boolean;
  income: number;
  totVotes: number;
  image: string;
  comments: IComment[]
}
