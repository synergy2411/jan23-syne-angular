export interface IComment {
  id: string;
  stars: number;
  body: string;
  author: string;
}
