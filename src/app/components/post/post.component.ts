import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IPost } from 'src/app/model/post.interface';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  postCollection: IPost[];

  constructor(
    private postService: PostService,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  private fetchPost() {
    this.postService.getPosts()
      .subscribe(data => this.postCollection = data)
  }

  ngOnInit(): void {
    this.fetchPost()
    this.route.queryParams.subscribe(query => {
      if (query && (query['deleted'] || query['created'])) {
        this.fetchPost()
      }
    })
  }

  onAddPost() {
    this.router.navigate(["/posts/add-post"])
  }
  postSelected(postId: string) {
    this.router.navigate([`/posts/${postId}`])
  }
}
