import { Component } from '@angular/core';
import { interval, Subscription, from, Observable, Subject } from 'rxjs';
import { take, filter, map, tap } from 'rxjs/operators';

@Component({
  selector: 'app-observable-demo',
  templateUrl: './observable-demo.component.html',
  styleUrls: ['./observable-demo.component.css']
})
export class ObservableDemoComponent {

  subject = new Subject()

  onResult() {
    this.subject.next("New Package")
  }

  onSubscribe() {
    this.subject.subscribe(data => console.log("Sub : 1 - ", data))
  }


  // interval$ = interval(1000);

  // onSubscribe() {
  //   this.interval$.pipe(
  //     take(5),
  //     tap(val => console.log("TAP : ", val)),
  //     filter(val => val > 2),
  //     map(val => val * 2),
  //   ).subscribe(console.log)
  // }


  // obs$ = new Observable(observer => {
  //   setTimeout(() => { observer.next("First Package") }, 1000)
  //   setTimeout(() => { observer.next("Second Package") }, 3000)
  //   setTimeout(() => { observer.next("Third Package") }, 5000)
  //   setTimeout(() => { observer.next("Fourth Package") }, 6000)
  //   setTimeout(() => { observer.complete() }, 8000)

  // })


  // friends = ["Joe", "Chandler", "Monica", "Ross"];

  // fromFriends$ = from(this.friends);

  // interval$ = interval(1000)

  // unsub$: Subscription;

  // onSubscribe() {
  //   // this.obs$.subscribe({
  //   //   next: data => console.log(data),
  //   //   error: err => console.log(err),
  //   //   complete: () => console.log("COMPLETED")
  //   // })

  // this.fromFriends$.subscribe((data) => { console.log(data) })

  // this.unsub$ = this.interval$.subscribe({
  //   next: data => console.log(data),
  //   error: err => console.log(err),
  //   complete: () => console.log("Completed")
  // })
  // }

  // onUnsubscribe() {
  //   this.unsub$.unsubscribe()
  // }

}
