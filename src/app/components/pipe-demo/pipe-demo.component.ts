import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe-demo',
  templateUrl: './pipe-demo.component.html',
  styleUrls: ['./pipe-demo.component.css']
})
export class PipeDemoComponent implements OnInit {

  promise = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve("Here the data arrived")
    }, 3000);
  })

  contactNumber = 987654321;

  todoCollection = [
    { label: "grocery", status: "completed" },
    { label: "shopping", status: "pending" },
    { label: "planting", status: "completed" },
    { label: "insurance", status: "pending" },
  ]

  filteredStatus = ''

  onAddItem() {
    this.todoCollection.push({ label: "New Item", status: "pending" })
    debugger;
  }
  constructor() { }

  ngOnInit(): void {
  }

}
