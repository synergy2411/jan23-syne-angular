import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css'],
  encapsulation: ViewEncapsulation.Emulated
})
export class DemoComponent {

  todoLabel = "to buy the jeans";
  todoStatus = "Completed"
}
