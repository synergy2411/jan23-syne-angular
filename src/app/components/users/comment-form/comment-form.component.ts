import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { v4 } from 'uuid';
import { NgForm } from '@angular/forms';
import { IComment } from 'src/app/model/comment.interface';

@Component({
  selector: 'app-comment-form',
  templateUrl: './comment-form.component.html',
  styleUrls: ['./comment-form.component.css']
})
export class CommentFormComponent implements OnInit {

  @Output() commentEvent = new EventEmitter<IComment>();
  constructor() { }

  ngOnInit(): void {
  }

  onAddNewComment(form: NgForm) {
    console.log(form.value);
    const newComment: IComment = {
      ...form.value,
      id: v4()
    }
    this.commentEvent.emit(newComment);
  }

}
