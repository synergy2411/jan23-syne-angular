import { Component, OnInit } from '@angular/core';

import { IUser } from 'src/app/model/user.interface';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: IUser[] = [];

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getUserData()
      .subscribe(data => this.users = data)
  }

  onMoreInfo(usr: IUser) {
    alert(`Who's this?`)
    console.log(usr);
  }

  onChangeVotes(inputEl: HTMLInputElement) {
    // this.user.totVotes = Number(inputEl.value);
  }

  onChildDemo(str: string) {
    console.log(str);
  }
}
