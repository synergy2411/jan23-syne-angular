import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IUser } from 'src/app/model/user.interface';

@Component({
  selector: 'app-user-image',
  templateUrl: './user-image.component.html',
  styleUrls: ['./user-image.component.css']
})
export class UserImageComponent {

  @Input() user: IUser | undefined;

  @Output() childEvent = new EventEmitter<IUser>()
  @Output() demoEvent = new EventEmitter()


  onButtonClick() {
    this.childEvent.emit(this.user)       // Event Data
  }

  onDemoClick() {
    this.demoEvent.emit("The Demo Event Data")
  }
}
