import { Component, Input } from '@angular/core';
import { IUser } from 'src/app/model/user.interface';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent {

  @Input() user: IUser | undefined;

  myClasses = {
    'my-border': true,
    'my-feature': false
  }

  myStyles = {
    color: '#fff',
    backgroundColor: "#444"
  }

  onMouseLeave() {
    this.myStyles.color = "#fff";
    this.myStyles.backgroundColor = "#444"
  }
  onChangeStyle() {
    this.myStyles.color = "#444";
    this.myStyles.backgroundColor = "#fff"
  }

  toggleClasses() {
    this.myClasses['my-border'] = !this.myClasses['my-border']
    this.myClasses['my-feature'] = !this.myClasses['my-feature']
  }
}
