import { Component, Input, OnInit } from '@angular/core';
import { IComment } from 'src/app/model/comment.interface';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  tab = 0;

  @Input() comments: IComment[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  addNewComment(comment: IComment) {
    this.comments.push(comment);
    this.tab = 1;
  }
}
