import { Component, ViewChild, ElementRef, ContentChild, Input, OnInit, SimpleChanges, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-life-cycle-demo',
  templateUrl: './life-cycle-demo.component.html',
  styleUrls: ['./life-cycle-demo.component.css']
})
export class LifeCycleDemoComponent implements
  OnInit,
  OnChanges,
  DoCheck,
  AfterContentInit,
  AfterContentChecked,
  AfterViewInit,
  AfterViewChecked,
  OnDestroy {

  constructor() { }

  @ViewChild("paragraphEl") paragraphElRef: ElementRef | undefined;

  @ContentChild("headerEl") headerElRef: ElementRef | undefined;

  show = true;        // onChanges will NOT fire on component model

  @Input() title: string = '';        // onChanges will Fire when Input decorated property will change


  ngOnChanges(changes: SimpleChanges) {
    console.log("ngOnChanges", changes);
  }

  ngAfterContentInit(): void {
    console.log("ngAfterContentInit");
    console.log("[ngAfterContentInit] Paragraph Element Ref : ", this.paragraphElRef)   // undefined
    console.log("[ngAfterContentInit] Header Element Ref : ", this.headerElRef)     // ElementRef
  }

  ngAfterViewInit(): void {
    console.log("ngAfterViewInit");
    console.log("[ngAfterViewInit] Paragraph Element Ref : ", this.paragraphElRef)    // ElementRef
    console.log("[ngAfterViewInit] Header Element Ref : ", this.headerElRef)          // undefined
  }

  ngOnInit(): void {
    console.log("ngOnInit");
    console.log("[ngOnInit] Paragraph Element Ref : ", this.paragraphElRef)     // undefined
    console.log("[ngOnInit] Header Element Ref : ", this.headerElRef)         // undefined
  }

  ngAfterContentChecked(): void {
    console.log("ngAfterContentChecked");
  }

  ngAfterViewChecked(): void {
    console.log("ngAfterViewChecked");
    console.log("[ngAfterViewChecked] Header Element Ref : ", this.headerElRef)
  }
  ngOnDestroy(): void {
    console.log("ngOnDestroy");
  }


  ngDoCheck(): void {
    console.log("ngDoCheck");
  }

}
