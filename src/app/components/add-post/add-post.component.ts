import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  constructor(
    private router: Router,
    private postService: PostService
  ) { }

  ngOnInit(): void {
  }

  onAddPost(form: NgForm) {
    const { title, body } = form.value;
    this.postService.createPost(title, body)
      .subscribe(data => {
        this.router.navigate(["/posts"], { queryParams: { created: true } })
      })
  }
  onCancel() {
    this.router.navigate(["/posts"])
  }
}
