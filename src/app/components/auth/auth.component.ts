import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
// import { MyValidators } from '../../shared/MyValidators';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  loginMessage = '';
  username = new FormControl('', [
    Validators.required,
    Validators.email
  ]);     // refers to the individual form element
  password = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
    this.hasExclamation
  ]);

  hobbies = new FormArray<FormGroup<any>>([]);

  authForm: FormGroup;           // refers to the complete form

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.authForm = this.fb.group({
      username: this.username,
      password: this.password,
      hobbies: this.hobbies
      // hobbies: this.fb.array([])
    })
  }

  // get hobbies() {
  //   return this.authForm.get("hobbies") as FormArray;
  // }
  addHobby() {
    let newHobby = this.fb.group({
      name: "",
      frequency: ""
    })
    this.hobbies.push(newHobby)
  }

  onDeleteHobby(index: number) {
    this.hobbies.removeAt(index)
  }

  onLogin() {
    const { username, password } = this.authForm.value;
    this.authService.onLogin(username, password)
      .subscribe(message => {
        this.router.navigate(["/users"])
      })
  }

  onRegister() {
    this.authService.onRegister({ ...this.authForm.value })
      .subscribe({
        next: data => console.log("User Registered", data),
        error: err => console.log(err)
      })
  }

  ngOnInit(): void {
  }

  hasExclamation(control: AbstractControl<any>) {
    const hasExcl = control.value.indexOf("!") >= 0;
    return hasExcl ? null : { needExclamation: true }
  }

}
