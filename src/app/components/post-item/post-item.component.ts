import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IPost } from 'src/app/model/post.interface';
import { PostService } from 'src/app/services/post.service';

@Component({
  selector: 'app-post-item',
  templateUrl: './post-item.component.html',
  styleUrls: ['./post-item.component.css']
})
export class PostItemComponent implements OnInit {

  post: IPost;

  constructor(
    private route: ActivatedRoute,
    private postService: PostService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.post = data['postData']
    })
    // this.route.params.subscribe(params => {
    //   const { postId } = params;
    //   this.postService.getPost(postId)        // 500ms
    //     .subscribe(post => {
    //       this.post = post
    //     })
    // })
  }

  onDelete(postId: string) {
    this.postService.deletePost(postId)
      .subscribe(data => {
        this.router.navigate(["/posts"], { queryParams: { deleted: true } })
      })
  }

  onCancel() {
    this.router.navigate(["/posts"])
  }

}
