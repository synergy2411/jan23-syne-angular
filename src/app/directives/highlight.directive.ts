import { Directive, ElementRef, HostBinding, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  @Input() favColor: string;

  @HostBinding("style.backgroundColor") bgColor: string;
  @HostBinding("style.font-size") fontSize: string;

  @HostListener('mouseenter')
  onMouseEnter() {
    this.bgColor = this.favColor || "blue";
    // this.fontSize = "2em";
    // this.elRef.nativeElement.style.backgroundColor = "#ff45ff"
  }

  @HostListener("mouseleave")
  onMouseLeave() {
    this.bgColor = "transparent";
    this.fontSize = "default";
    // this.elRef.nativeElement.style.backgroundColor = "transparent"
  }

  // Dependency/Constructor Injection
  constructor(private elRef: ElementRef) {
    // this.elRef.nativeElement.style.backgroundColor = "#ff45ff";
  }

}

// <div appHighlight> </div>

// <div class="highlight"></div>

// .highlight{}

// <div name="highlight"> </div>

// [name="highlight"]{

// }
