import { AfterViewInit, Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[test]'
})
export class TestDirective implements AfterViewInit {

  @Input() test: string;

  @Input() textSize: string;

  constructor(private elRef: ElementRef) { }

  ngAfterViewInit(): void {
    this.elRef.nativeElement.style.fontSize = this.textSize;
  }

}
