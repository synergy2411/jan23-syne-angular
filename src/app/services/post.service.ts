import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IPost } from '../model/post.interface';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private baseURL = "http://localhost:3000/posts"
  constructor(private http: HttpClient) { }

  getPosts() {
    return this.http.get<IPost[]>(this.baseURL)
  }

  getPost(postId: string) {
    return this.http.get<IPost>(`${this.baseURL}/${postId}`)
  }

  deletePost(postId: string) {
    return this.http.delete(`${this.baseURL}/${postId}`)
  }

  createPost(title: string, body: string) {
    return this.http.post(this.baseURL, { title, body })
  }
}
