import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router'
import { hashSync, compareSync } from 'bcryptjs'
import { IUser } from '../model/user.interface';
import { Observable } from 'rxjs';

interface UserDetailsType {
  username: string;
  password: string;
  hobbies: Array<{ name: string, frequency: number }>
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private isLoggedIn: boolean | null = null;

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  onRegister(userDetails: UserDetailsType) {
    const { password } = userDetails;
    const hashedPassword = hashSync(password, 12)
    return this.http.post("http://localhost:3000/admin", { ...userDetails, password: hashedPassword })
  }

  onLogin(username: string, password: string): Observable<string> {
    return new Observable((observer) => {
      this.http.get<UserDetailsType[]>(`http://localhost:3000/admin?username=${username}`)
        .subscribe(users => {
          if (users.length > 0) {
            const isMatch = compareSync(password, users[0].password)
            if (isMatch) {
              this.isLoggedIn = true;
              return observer.next("Logged In")
            }
          }
          return observer.next("Bad Credentials")
        })
    })
  }

  isUserAuthenticated() {
    return this.isLoggedIn !== null;
  }

  logout() {
    setTimeout(() => {
      this.isLoggedIn = null;
      this.router.navigate(["/auth"])
    }, 1500)
  }
}
