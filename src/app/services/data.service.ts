import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { IUser } from '../model/user.interface';

@Injectable({
  providedIn: "root"
})
export class DataService {

  constructor(private http: HttpClient) { }

  getUserData() {
    return this.http.get<IUser[]>("http://localhost:3000/users")
  }
}
