import {
  HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class LoggerInterceptor implements HttpInterceptor {

  constructor(private router: Router) { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    console.log("[LOGGER INTERCEPTOR]", request);
    const clonedRequest = request.clone()
    return next.handle(clonedRequest).pipe(
      tap(val => console.log("TAP : ", val)),
      catchError((error: HttpErrorResponse) => {
        this.router.navigate(["/posts"])
        return of(null)
      })
    );
  }
}
