import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countryCode'
})
export class CountryCodePipe implements PipeTransform {

  transform(value: number, code?: string): string {
    switch (code) {
      case "USA": return "+1 " + value
      case "AUS": return "+2 " + value
      case "EUR": return "+23 " + value
      default: return "+91 " + value;
    }
  }

}

// Generate a pipe that will reverse the string

// interface Animal{
//   species : string;
//   legs? : number;
// }
// let rabbit : Animal = {
//   species : "Rabbit"
// }

